"""DSM VALIDATION SCRIPT - QRADAR."""
import sys
import xml.etree.ElementTree as ET

file_path, errors, root = None, [], None


def parseXML(xmlfile):
    """Parse XML."""
    tree = ET.parse(xmlfile)
    root = tree.getroot()
    return root


def check_for_language_tag():
    """Check whether <language> tag is present in every CEP and it must be en-US."""
    for item in root.findall("./ariel_regex_property"):
        property_name = item.find("propertyname").text
        if item.find("languagetag") is None:
            errors.append("language tag not present for property = {0}".format(property_name))
            continue
        if not "en-US" == item.find("languagetag").text:
            errors.append("language is not english for property = {0}".format(property_name))


def check_for_description():
    """Check whether <description> tag is present or not empty in the CEP."""
    for item in root.findall("./ariel_regex_property"):
        property_name = item.find("propertyname").text
        if item.find("description") is None:
            errors.append("Description tag not present for property = {0}".format(property_name))
            continue
        if item.find("description").text is None:
            errors.append("Description is empty for property = {0}".format(property_name))
            continue

        if not str(item.find("description").text.encode("utf8")).strip():
            errors.append("Description is empty for property = {0}".format(property_name))


def check_for_orphaned_property_or_expression():
    property_id_dict = {}
    for item in root.findall("ariel_regex_property"):
        ap_id = item.find("id").text
        if ap_id not in property_id_dict:
            property_id_dict[ap_id] = 1
        else:
            property_id_dict[ap_id] = property_id_dict[ap_id] + 1

    for each in ["ariel_property_json_expression", "ariel_property_expression", "ariel_property_cef_expression"]:
        for item in root.findall(each):
            ap_id = item.find("ap_id").text
            if ap_id not in property_id_dict:
                property_id_dict[ap_id] = 1
            else:
                property_id_dict[ap_id] = property_id_dict[ap_id] + 1

    for each_key in property_id_dict:
        if property_id_dict[each_key] < 2:
            errors.append("Found orphaned ap_id = {0}".format(each_key))


def check_for_orphaned_qid():
    qid_id_dict = {}
    for item in root.findall("dsmevent"):
        ap_id = item.find("qidmapid").text
        if ap_id not in qid_id_dict:
            qid_id_dict[ap_id] = 1
        else:
            qid_id_dict[ap_id] = qid_id_dict[ap_id] + 1

    for item in root.findall("qidmap"):
        ap_id = item.find("id").text
        if ap_id not in qid_id_dict:
            qid_id_dict[ap_id] = 1
        else:
            qid_id_dict[ap_id] = qid_id_dict[ap_id] + 1

    for each_key in qid_id_dict:
        if qid_id_dict[each_key] < 2:
            errors.append("Found orphaned qidmapid/qid = {0}".format(each_key))


def check_for_duplicate_event_name():
    event_name_list = []
    for item in root.findall("qidmap"):
        event_name = item.find("qname").text.strip()
        if event_name not in event_name_list:
            event_name_list.append(event_name)
        else:
            errors.append("Duplicate event name found. Event name = {0}".format(event_name))


def check_for_duplicate_dsmevent_id():
    event_name_list = []
    for item in root.findall("dsmevent"):
        event_name = item.find("id").text.strip()
        if event_name not in event_name_list:
            event_name_list.append(event_name)
        else:
            errors.append("Duplicate dsmevent ID found. Event name = {0}".format(event_name))


def check_for_duplicate_qidmap_id():
    event_name_list = []
    for item in root.findall("qidmap"):
        event_name = item.find("qid").text.strip()
        if event_name not in event_name_list:
            event_name_list.append(event_name)
        else:
            errors.append("Duplicate qidmap ID found. Event name = {0}".format(event_name))


def check_for_device_id_in_property():
    for each in ["ariel_property_json_expression", "ariel_property_expression"]:
        for item in root.findall(each):
            ap_id = item.find("ap_id").text
            if item.find("deviceid").text != "-1":
                errors.append("Device is not -1 for ap_id {0}".format(ap_id))


def check_for_sample_payload():
    """Check the payload tag in the CEP. It should not be empty"""
    for each in ["ariel_property_json_expression", "ariel_property_expression"]:
        for item in root.findall(each):
            ap_id = item.find("ap_id").text
            if item.find("payload") is None:
                errors.append("Payload tag not present for ap_id = {0}".format(ap_id))
                continue
            if not str(item.find("payload").text).strip():
                errors.append("<payload> tag is empty for ap_id = {0}".format(ap_id))


def check_for_not_allowed_property_name():
    NOT_ALLOWED_PROPERTY_NAME = ["count", "domain", "protocol", "severity"]
    for item in root.findall("ariel_regex_property"):
        property_name = item.find("propertyname").text
        if str(property_name).lower() in NOT_ALLOWED_PROPERTY_NAME:
            errors.append("Property name {0} not allowed.".format(property_name))


def check_for_required_tags():
    required_tag = [
        "ariel_regex_property",
        "sensordevicetype",
        "sensordevicecategory",
        "device_ext",
        "sensordeviceprotocols",
        "sensordevice",
    ]
    for each in required_tag:
        if root.find(each) is None:
            errors.append("<{0}> tag is not present in XML.".format(each))


def check_for_device_id():
    for each in ["ariel_property_json_expression", "ariel_property_expression", "ariel_property_cef_expression"]:
        for item in root.findall(each):
            ap_id = item.find("ap_id").text
            device_id = item.find("deviceid").text
            if device_id != "-1":
                errors.append("Device ID is not -1 for {0}".format(ap_id))


def check_for_duplicate_sequence_id():
    for each in ["ariel_regex_property", "ariel_property_json_expression"]:
        seq_id_list = []
        for item in root.findall(each):
            seq_id = item.find("sequenceid").text
            if seq_id in seq_id_list:
                errors.append("Duplicate <sequenceid> found {0} for {1}".format(seq_id, each))
            seq_id_list.append(seq_id)


def check_for_duplicate_dsmevent_uuid():
    event_name_list = []
    for item in root.findall("dsmevent"):
        event_name = item.find("uuid").text.strip()
        if event_name not in event_name_list:
            event_name_list.append(event_name)
        else:
            errors.append("Duplicate dsmevent UUID found. Event name = {0}".format(event_name))


def exit(failed=False):
    sys.exit(-1 if failed else 0)


def main():
    try:
        global root
        global file_path
        file_path = sys.argv[1]
        root = parseXML(file_path)
        check_for_required_tags()
        check_for_not_allowed_property_name()
        check_for_language_tag()
        check_for_description()
        check_for_sample_payload()
        check_for_orphaned_property_or_expression()
        check_for_device_id_in_property()
        check_for_orphaned_qid()
        check_for_duplicate_event_name()
        check_for_duplicate_dsmevent_id()
        check_for_duplicate_qidmap_id()
        check_for_device_id()
        check_for_duplicate_sequence_id()
        check_for_duplicate_dsmevent_uuid()

        if len(errors) > 0:
            print("=============================================")
            print("DSM has following errors:")
            for e in errors:
                print(e)
            print("=============================================")
            exit(True)
        else:
            print("=============================================")
            print("DSM IS OK!!!")
            print("=============================================")
            exit()
    except Exception as e:
        print("=============================================")
        print("Error occurred while validating DSM.")
        print(e)
        print("=============================================")
        exit(True)


if __name__ == "__main__":
    main()

# TODO add for checking unnecessary CEP, and Event mapping which are not linked with sensordevicetype
# TODO Add for printint overall summary like total CEP, total event mapping etc..
